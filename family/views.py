
import email
from django.shortcuts import render
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from rest_framework.response import Response 
from datetime import timedelta
from django.utils import timezone
from django.conf import settings  
from rest_framework.generics import GenericAPIView
from .models import CustomeUser


@csrf_exempt  
@api_view(["POST"])
@permission_classes((AllowAny,))
def get_token(request):
    username  = request.data.get("username")  
    print("The usename is",username)
    user = CustomeUser.objects.get(username=username) 
    if user:
        token = Token.objects.get(user=user.id) 
        data = {
            "token":token.key
        }
    return Response(data, status=HTTP_200_OK) 
    

@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")
    if username is None or password is None:
        return Response({'error': 'Please provide both username and password'},
                        status=HTTP_400_BAD_REQUEST)
    user = authenticate(username=username, password=password)
    if not user:
        return Response({'error': 'Invalid Credentials'},
                        status=HTTP_404_NOT_FOUND)
    token, _ = Token.objects.get_or_create(user=user) 
    is_expired, token = token_expire_handler(token) 
    return Response({'token': token.key,
                    'expires_in': expires_in(token),
                    },
                    status=HTTP_200_OK)


#this return left time
def expires_in(token):
    time_elapsed = timezone.now() - token.created
    left_time = timedelta(seconds = settings.TOKEN_EXPIRED_AFTER_SECONDS) - time_elapsed
    return left_time

# token checker if token expired or not
def is_token_expired(token):
    return expires_in(token) < timedelta(seconds = 0)

# if token is expired new token will be established
# If token is expired then it will be removed
# and new one with different key will be created
def token_expire_handler(token):
    is_expired = is_token_expired(token)
    if is_expired:
        token.delete()
        token = Token.objects.create(user = token.user)
    return is_expired, token 


@csrf_exempt
@api_view(["GET"])
def sample_api(request):
    data = {'sample_data': 123}
    return Response(data, status=HTTP_200_OK) 

    
class Register(GenericAPIView):   
    permission_classes = (AllowAny,)
    def post(self,request,*args,**kwargs): 
        email = self.request.POST.get("email")
        first_name = self.request.POST.get("first_name") 
        last_name = self.request.POST.get("last_name") 
        username = self.request.POST.get("username") 
        user_type = self.request.POST.get("user_type")
        password = self.request.POST.get("password") 

        user = CustomeUser.objects.create(
            username=username,
            email=email,
            first_name=first_name,
            last_name=last_name,
            is_head = True,
            user_type = user_type
        )
        user.set_password(password)
        user.save()    
        return Response()
